<?php
declare(strict_types=1);

use App\Domain\Money\Bank;
use App\Domain\Money\Expression;
use App\Domain\Money\Money;
use App\Domain\Money\Sum;
use Tests\TestCase;

class MoneyTest extends TestCase {


    /** @test */
    public function testMultiplication()
    {
        $five = Money::dollar(5);
        $this->assertTrue(Money::dollar(10)->equals($five->times(2)));
        $this->assertTrue(Money::dollar(15)->equals($five->times(3)));
    }

    /** @test */
    public function testFrancMultiplication()
    {
        $five = Money::franc(5);
        $this->assertTrue(Money::franc(10)->equals($five->times(2)));
        $this->assertTrue(Money::franc(15)->equals($five->times(3)));
    }

    /** @test */
    public function testEquality()
    {
        $this->assertTrue((Money::dollar(5))->equals(Money::dollar(5)));
        $this->assertFalse((Money::dollar(5))->equals(Money::dollar(6)));

        $this->assertTrue((Money::franc(5))->equals(Money::franc(5)));
        $this->assertFalse((Money::franc(5))->equals(Money::franc(6)));
        
        $this->assertFalse((Money::franc(5))->equals(Money::dollar(5)));
        
        $this->assertTrue((Money::real(5))->equals(Money::real(5)));
        $this->assertFalse((Money::real(5))->equals(Money::dollar(5)));
    }

    /** @test */
    public function testCurrency()
    {
        $this->assertEquals('USD', (Money::dollar(1))->currency());
        $this->assertEquals('CHF', (Money::franc(1))->currency());
        $this->assertEquals('BRR', (Money::real(1))->currency());
    }

    /** @test */
    public function testSimpleAddition()
    {
        $five = Money::dollar(5);
        $sum = $five->plus(Money::dollar(5)); // Expression
        $bank = new Bank();
        $reduced = $bank->reduce($sum, 'USD');
        $this->assertEquals(Money::dollar(10), $reduced);
    }

    /** @test */
    public function testPlusReturnsSum()
    {
        $five = Money::dollar(5);
        $result = $five->plus($five); // Expression
        $sum = $result;
        $this->assertEquals($five, $sum->augend);
        $this->assertEquals($five, $sum->addend);
    }

    /** @test */
    public function testReduceSum()
    {
        $sum = new Sum(Money::dollar(3), Money::dollar(4));
        $bank = new Bank();
        $result = $bank->reduce($sum, 'USD');
        $this->assertEquals(Money::dollar(7), $result);  
    }
    
    /** @test */
    public function testReduceMoney()
    {
        $bank = new Bank();
        $result = $bank->reduce(Money::dollar(1), 'USD');
        $this->assertEquals(Money::dollar(1), $result);
    }
    
    /** @test */
    public function testReduceMoneyDifferentCurrency()
    {
        $bank = new Bank();
        $bank->addRate('CHF', 'USD', 2);
        $result = $bank->reduce(Money::franc(2), 'USD');
        $this->assertEquals(Money::dollar(1), $result);
    }

    /** @test */
    public function testIdentityRate()
    {
        $this->assertEquals(1, (new Bank())->rate('USD', 'USD'));
    }

    /** @test */
    public function testMixedAddition()
    {
        $fiveDollars = Money::dollar(5);
        $tenFrancs = Money::franc(10);
        $fourtyReals = Money::real(40);
        $bank = new Bank();
        $bank->addRate('CHF', 'USD', 2);
        $bank->addRate('BRR', 'USD', 4);
        $result = $bank->reduce($fiveDollars->plus($tenFrancs), 'USD');
        $this->assertEquals(Money::dollar(10), $result);

        $result = $bank->reduce($fiveDollars->plus($fourtyReals), 'USD');
        $this->assertEquals(Money::dollar(15), $result);
    }

    /** @test */
    public function testSumPlusMoney()
    {
        $fiveDollars = Money::dollar(5);
        $tenFrancs = Money::franc(10);
        $bank = new Bank();
        $bank->addRate('CHF', 'USD', 2);
        $sum = (new Sum($fiveDollars, $tenFrancs))->plus($fiveDollars);
        $result = $bank->reduce($sum, 'USD');
        $this->assertEquals(Money::dollar(15), $result);
    }

    /** @test */
    public function testSumTimes()
    {
        $fiveDollars = Money::dollar(5);
        $tenFrancs = Money::franc(10);
        $bank = new Bank();
        $bank->addRate('CHF', 'USD', 2);
        $sum = (new Sum($fiveDollars, $tenFrancs))->times(2);
        $result = $bank->reduce($sum, 'USD');
        $this->assertEquals(Money::dollar(20), $result);
    }
    
    /** @test */
    public function testPlusSameCurrencyReturnsMoney()
    {
        $sum = Money::dollar(1)->plus(Money::dollar(1));
        $this->assertTrue($sum instanceof Sum);
    }
    
    
    
    
    
}