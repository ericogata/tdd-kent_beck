<?php 

namespace App\Domain\Money;

class Money implements Expression{

    protected $amount;
    protected $currency;

    function __construct(int $amount, string $currency)
    {
        $this->amount   = $amount;
        $this->currency = $currency; 
    }
    /**
     * Valida dois valores de Dollar, assim como 
     * se os dois objectos possuem o mesmo tipo de classe.
     * @return boolean true if two Dollars are equal
     */
    public function equals(Money $money) : bool
    {
        return $this->amount == $money->amount
            && $this->currency() == $money->currency();
    }

    /**
     * @return string Retorna uma string com caracteres representando o tipo de moeda
     */
    public function currency() : string
    {
        return $this->currency;
    }

    public function amount() : int
    {
        return $this->amount;
    }

    // Geração de moedas
    public static function dollar(int $amount) : Money
    {
        return new Money($amount, 'USD');
    }

    public static function franc(int $amount) : Money
    {
        return new Money($amount, 'CHF');
    }

    public static function real(int $amount) : Money
    {
        return new Money($amount, 'BRR');
    }
    // ************************************

    /**
     * @return Money nova instância de Dollar/Franc com o produto da multiplação
     */
    public function times(int $multipler = 1) : Expression
    {
        return new Money($this->amount * $multipler, $this->currency);
    }
    

    public function toString() : string {
        return $this->amount . " " . $this->currency;
    }

    public function plus(Expression $addend) : Expression
    {
        return new Sum($this, $addend);
    }

    public function reduce(Bank $bank, string $to) : Money
    {
        $rate = $bank->rate($this->currency, $to);
        return new Money($this->amount / $rate, $to);
    }
}