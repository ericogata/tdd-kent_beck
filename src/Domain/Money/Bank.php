<?php 
namespace App\Domain\Money;

class Bank {

    private $rates = array();

    public function reduce(Expression $source, string $to) : Money
    {
        return $source->reduce($this, $to);
    }

    public function rate(string $from, string $to) : int
    {
        if($from === $to) return 1;
        return $this->rates[(new Pair($from, $to))->__toString()];
    }

    public function addRate(string $from, string $to, int $rate)
    {
        $this->rates += [(new Pair($from, $to))->__toString() => $rate];
    }


}