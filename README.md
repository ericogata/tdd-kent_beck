# TDD-Kent_Beck

Projeto em PHP, executando os exercícios do livro TDD: Desenvolvimento Dirigido por Teste, de Kent Beck. Os códigos originais eram em Java/Python.

### Prerequisites

* PHP7
* Composer

### Installing

Faça uma clonagem do repositório na sua máquina, e, na raiz do projeto, execute o composer:

```
composer install
```

## Running the tests

Existem duas maneiras de rodas os códigos:

### Teste Total
Na raiz do projeto:
```
vendor/phpunit/phpunit/phpunit
```
Output:
```
PHPUnit 7.5.17 by Sebastian Bergmann and contributors.

..............                                                    14 / 14 (100%)

Time: 22 ms, Memory: 4.00 MB

OK (14 tests, 26 assertions)
```

### Teste Filtrado
Utilizando a opção "--filter <classe/função>", podemos rodar um código específico no sistema. 

Na raiz do projeto:
```
vendor/phpunit/phpunit/phpunit --filter testMixedAddition
```
Output:
```
PHPUnit 7.5.17 by Sebastian Bergmann and contributors.

.                                                                   1 / 1 (100%)

Time: 22 ms, Memory: 4.00 MB
```

## Built With

* [PHP7](http://www.dropwizard.io/1.0.2/docs/);
* [SLIM](http://www.slimframework.com/) - Micro Framework PHP para criação rápida de aplicações web e APIs;
* [PHPUnit](https://phpunit.de/) - Framework de teste em PHP baseado na arquitetura xUnit;

## Authors

* **Eric Ogata @ WebHub** - *Initial work* - [EricOgata@WebHub](http://www.webhub.com.br/)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Kent Beck, pelo livro TDD
